const { merge } = require("webpack-merge");
const singleSpaDefaults = require("webpack-config-single-spa-react-ts");

module.exports = (webpackConfigEnv, argv) => {
  const defaultConfig = singleSpaDefaults({
    orgName: "plc",
    projectName: "main",
    webpackConfigEnv,
    argv,
  });

  return merge(defaultConfig, {
    externals: ["@mui/material", "react-icons", "react-router-dom"],
  });
};
