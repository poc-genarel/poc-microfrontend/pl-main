import { useAppDispatch, useAppSelector } from "../redux/store";
import { setUser } from "../redux/slices/userSlice";
import { useEffect } from "react";

export const useUserAuth = () => {
  const dispatch = useAppDispatch();
  const { user } = useAppSelector((state) => state.user);

  useEffect(() => {
    emitUpdateUser(user);
  }, [user]);

  const login = () => {
    dispatch(setUser({ login: "alessandro", name: "Alessandro Santos" }));
  };

  const logout = () => {
    dispatch(setUser(null));
  };

  const isAuthenticated = () => {
    return !!user;
  };

  const emitUpdateUser = (user: any) => {
    const event = new CustomEvent("@plc/main/updateUser", {
      detail: user,
    });
    window.dispatchEvent(event);
  };

  return {
    user,
    login,
    logout,
    isAuthenticated,
  };
};
