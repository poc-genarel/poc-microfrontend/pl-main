import { styled } from "@mui/material";
import { config } from "../config";

interface ContentProps {
  open: boolean;
}

export const Content = styled("div")<ContentProps>(({ theme, open }) => {
  const contentWidth = open ? config.drawerWidth : 0;
  return {
    position: "absolute",
    right: 0,
    bottom: 0,
    display: "flex",
    boxSizing: "border-box",
    width: `calc(100vw - ${contentWidth}px)`,
    height: `calc(100vh - ${config.headerBarHeight}px)`,
    transition: theme.transitions.create(["width"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  };
});
