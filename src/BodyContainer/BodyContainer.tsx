import React from "react";
import { Content } from "./styles";
import { Route, Switch } from "react-router-dom";
import Parcel from "single-spa-react/parcel";
import menuItems from "../MenuItems";

interface BodyContainerProps {
  isDrawerOpen: boolean;
}

const BodyContainer = ({ isDrawerOpen }: BodyContainerProps) => {
  return (
    <Content open={isDrawerOpen}>
      <Switch>
        {menuItems.map((item) =>
          item.parcelName ? (
            <Route key={item.name} path={item.pathname}>
              <Parcel config={() => System.import(item.parcelName)} />
            </Route>
          ) : null
        )}
        <Route path={"*"}>
          <p>Não foi possivel encontrar nada</p>
        </Route>
      </Switch>
    </Content>
  );
};

export default BodyContainer;
