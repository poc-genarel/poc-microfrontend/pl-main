import { AppBar as MuiAppBar, styled } from "@mui/material";
import { config } from "../config";

interface AppBarProps {
  open: boolean;
}

export const AppBar = styled(MuiAppBar)<AppBarProps>(({ theme, open }) => {
  const drawerWidth = open ? config.drawerWidth : 0;

  return {
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  };
});
