import React from "react";
import { Box, Button, IconButton, Toolbar, Typography } from "@mui/material";
import { FaBars, FaUser } from "react-icons/fa";
import { AppBar } from "./styles";
import { useUserAuth } from "../hook/useUserAuth";

interface HeaderBarProps {
  toggleDrawer: () => void;
  isDrawerOpen: boolean;
}

const HeaderBar = ({ isDrawerOpen, toggleDrawer }: HeaderBarProps) => {
  const { isAuthenticated, login, logout, user } = useUserAuth();

  return (
    <AppBar position="fixed" open={isDrawerOpen}>
      <Toolbar variant={"dense"}>
        <IconButton
          color="inherit"
          aria-label="open drawer"
          onClick={toggleDrawer}
          edge="start"
          sx={{
            marginRight: "36px",
          }}
        >
          <FaBars />
        </IconButton>
        <Typography variant="h6" noWrap component="div" sx={{ flexGrow: 1 }}>
          Mini variant drawer
        </Typography>
        {isAuthenticated() ? (
          <Box display={"flex"} flexDirection={"row"} alignItems={"center"}>
            <IconButton onClick={logout} color={"inherit"}>
              <FaUser />
            </IconButton>
            <Typography>{user.name}</Typography>
          </Box>
        ) : (
          <Button variant={"outlined"} color={"inherit"} onClick={login}>
            Login
          </Button>
        )}
      </Toolbar>
    </AppBar>
  );
};

export default HeaderBar;
