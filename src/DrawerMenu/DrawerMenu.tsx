import React, { Fragment, ReactNode } from "react";
import { List } from "@mui/material";
import { Drawer } from "./styles";
import menuItems, {
  MenuItems,
  MenuItemsGroup,
  MenuItemsSingle,
} from "../MenuItems";
import CustomListItemSingle from "./CustomListItem/CustomListItemSingle";
import CustomListItemGroup from "./CustomListItem/CustomListItemGroup";

interface DrawerMenuProps {
  isDrawerOpen: boolean;
}

const DrawerMenu = ({ isDrawerOpen }: DrawerMenuProps) => {
  const getListItem = (item: MenuItems) => {
    const mapListItem: Record<
      MenuItems["type"],
      (item: MenuItems) => ReactNode
    > = {
      single: (item: MenuItemsSingle) => (
        <CustomListItemSingle key={item.pathname} item={item} />
      ),
      group: (item: MenuItemsGroup) => (
        <CustomListItemGroup
          item={{
            ...item,
          }}
          groupItems={item.items}
        />
      ),
    };

    return <Fragment key={item.name}>{mapListItem[item.type](item)}</Fragment>;
  };

  return (
    <Drawer anchor={"left"} open={isDrawerOpen} variant={"persistent"}>
      <List>{menuItems.map(getListItem)}</List>
    </Drawer>
  );
};

export default DrawerMenu;
