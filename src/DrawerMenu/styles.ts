import { styled } from "@mui/material";
import { Drawer as MuiDrawer } from "@mui/material";
import { config } from "../config";

export const Drawer = styled(MuiDrawer)(({ open }) => {
  return {
    "& .MuiDrawer-paper": {
      width: config.drawerWidth,
    },
  };
});
