import React, { ElementType, ReactNode } from "react";
import { FaInfo } from "react-icons/fa";
import {
  ListItem,
  ListItemProps,
  ListItemIcon,
  ListItemText,
  ListItemTypeMap,
} from "@mui/material";

interface CustomListItemProps<C> {
  label: string;
  icon: ReactNode;
  ListItemButtonProps?: {
    component?: ElementType<C>;
  } & ListItemProps<"li", C>;
  endIcon?: ReactNode;
}

const CustomListItem = <C,>({
  icon,
  label,
  ListItemButtonProps,
  endIcon,
}: CustomListItemProps<C>) => {
  const getIcon = () => {
    return icon ?? <FaInfo />;
  };

  return (
    <ListItem button {...ListItemButtonProps}>
      <ListItemIcon>{getIcon()}</ListItemIcon>
      <ListItemText primary={label} />
      {endIcon}
    </ListItem>
  );
};

export default CustomListItem;
