import React from "react";
import { MenuItemsCommon } from "../../MenuItems";
import { FaInfo } from "react-icons/fa";
import { Link } from "react-router-dom";
import CustomListItem from "./CustomListItem";

interface CustomListItemProps {
  item: MenuItemsCommon;
}

const CustomListItemSingle = ({ item }: CustomListItemProps) => {
  return (
    <CustomListItem
      label={item.label}
      icon={item.icon}
      ListItemButtonProps={{
        key: item.name,
        component: Link,
        to: item.pathname,
      }}
    />
  );
};

export default CustomListItemSingle;
