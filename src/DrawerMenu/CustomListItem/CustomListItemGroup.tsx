import React, { Fragment, useState } from "react";
import { MenuItemsCommon } from "../../MenuItems";
import { Collapse, List } from "@mui/material";
import CustomListItemSingle from "./CustomListItemSingle";
import CustomListItem from "./CustomListItem";
import { Arrow } from "./styles";

interface CustomListItemGroupProps {
  item: MenuItemsCommon;
  groupItems: MenuItemsCommon[];
}

const CustomListItemGroup = ({
  item: { label, icon, name, pathname },
  groupItems,
}: CustomListItemGroupProps) => {
  const [collapseOpen, setCollapseOpen] = useState(false);
  const toggleCollapse = () => {
    setCollapseOpen(!collapseOpen);
  };

  return (
    <Fragment>
      <CustomListItem
        label={label}
        icon={icon}
        ListItemButtonProps={{ onClick: toggleCollapse }}
        endIcon={<Arrow isDown={!collapseOpen} />}
      />
      <Collapse in={collapseOpen} timeout="auto" unmountOnExit>
        <List disablePadding style={{ marginLeft: 10 }}>
          {groupItems.map((item) => (
            <CustomListItemSingle
              item={{ ...item, pathname: pathname + item.pathname }}
              key={name + "-" + item.name}
            />
          ))}
        </List>
      </Collapse>
    </Fragment>
  );
};

export default CustomListItemGroup;
