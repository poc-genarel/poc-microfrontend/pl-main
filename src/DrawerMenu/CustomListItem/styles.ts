import { styled } from "@mui/material";
import { FaCaretDown } from "react-icons/fa";

interface ArrowProps {
  isDown: boolean;
}

export const Arrow = styled(FaCaretDown)<ArrowProps>(({ theme, isDown }) => {
  return {
    transform: !isDown ? "rotate(180deg)" : undefined,
    transition: theme.transitions.create(["transform"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  };
});
