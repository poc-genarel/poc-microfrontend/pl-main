import { ReactNode } from "react";
import { FaCog, FaCube, FaSearch } from "react-icons/fa";

export interface MenuItemsCommon {
  name: string;
  pathname: string;
  icon: ReactNode;
  label: string;
}

export interface MenuItemsSingle extends MenuItemsCommon {
  type: "single";
  parcelName: string;
}

export interface MenuItemsGroup extends MenuItemsCommon {
  type: "group";
  parcelName: string;
  items: MenuItemsCommon[];
}

export type MenuItems = MenuItemsSingle | MenuItemsGroup;

const menuItems: MenuItems[] = [
  {
    type: "group",
    name: "mesa-operacao",
    icon: <FaCube />,
    label: "Mesa de Operações",
    pathname: "/mesa-operacao",
    parcelName: "@plc/mesa-operacao",
    items: [
      {
        label: "Consulta",
        icon: <FaSearch />,
        name: "consulta",
        pathname: "/consulta",
      },
      {
        label: "Configuração",
        icon: <FaCog />,
        name: "configuracao",
        pathname: "/configuracao",
      },
    ],
  },
  {
    type: "single",
    name: "test",
    label: "Teste",
    icon: null,
    pathname: "/test",
    parcelName: null,
  },
];

export default menuItems;
