import { createTheme } from "@mui/material";
import { config } from "./config";

export const customTheme = createTheme({
  mixins: {
    toolbar: {
      height: config.headerBarHeight,
    },
  },
});
