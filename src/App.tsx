import React, { useState } from "react";
import { ThemeProvider } from "@mui/material";
import DrawerMenu from "./DrawerMenu";
import HeaderBar from "./HeaderBar";
import BodyContainer from "./BodyContainer";
import { customTheme } from "./customTheme";
import { BrowserRouter } from "react-router-dom";
import { Provider as ReduxProvider } from "react-redux";
import { store } from "./redux/store";

const App = () => {
  const [openDrawer, setOpenDrawer] = useState(false);

  const toggleDrawer = () => {
    setOpenDrawer(!openDrawer);
  };

  return (
    <ThemeProvider theme={customTheme}>
      <ReduxProvider store={store}>
        <BrowserRouter>
          <HeaderBar isDrawerOpen={openDrawer} toggleDrawer={toggleDrawer} />
          <DrawerMenu isDrawerOpen={openDrawer} />
          <BodyContainer isDrawerOpen={openDrawer} />
        </BrowserRouter>
      </ReduxProvider>
    </ThemeProvider>
  );
};

export default App;
