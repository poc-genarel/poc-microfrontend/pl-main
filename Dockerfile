FROM node:16-alpine as build

WORKDIR /app

COPY package*.json .

RUN npm install

COPY . .

ENV NODE_ENV production

RUN npm run build

FROM nginx:alpine

ARG context
EXPOSE 80
COPY --from=build /app/dist /usr/share/nginx/html/${context}/main
